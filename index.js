const   http    = require("http"); 
const   port    = process.env.PORT || 8080; 

var     express = require("express"); 
var     app     = express(); 

app.use(express.static('.'));
app.use('/css',     express.static(__dirname + 'css')); 
app.use('/js',      express.static(__dirname + 'js')); 
app.use('/images',  express.static(__dirname + 'images')); 

const server = app.listen(port, function()
{
    console.log("Server started at port %s.", port); 
}); 
