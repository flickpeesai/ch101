## README ##

Source code for Early Christian History (CH101) map available at:  
http://ch101.azurewebsites.net  
  
Main files for the map:  
- index.html , landing page  
- timeline.js , helper JS library  
- data.json  , GeoJSON data  
- data.html , page for composing new JSON data  
  
Files that might be useful if serving map using Node.js + nginx:  
- index.js , main routing file  
- package.json , for installing Express.js  
- web.config , helpful if hosting on Azure  
  
Made available under the Apache 2.0 licence.  
